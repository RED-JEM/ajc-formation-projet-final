﻿
using Stream.api;


static void Affiche<T>(string texte, IEnumerable<T> éléments)
{
    Console.WriteLine(texte.PadRight(50, '-'));
    foreach (T élément in éléments)
    {
        Console.WriteLine(élément);
    }
}

//exemple d'un enregistrement de fichier abonnee (viewer)
string[] info = { "Name: Felica Walker", "Title: Mz.",
                "Age: 47", "Location: Paris", "Gender: F"};
int found = 0;

Console.WriteLine("Les valeurs initiales du tableau sont :");

foreach (string s in info)
Console.WriteLine(s);

Console.WriteLine("\n Nous voulons récupérer uniquement les informations clés. C'est-à-dire:");

foreach (string s in info)
    {
        found = s.IndexOf(": ");
        Console.WriteLine("   {0}", s.Substring(found + 2));
    }

// création d'un dictionnaire <string,int>
string[] liste = { "jean:20", "paul:18", "mélanie:10", "violette:15" };
string[] champs = null;
char[] séparateurs = new char[] { ':' };

Dictionary<string, int> dico = new Dictionary<string, int>();
    for (int i = 0; i < liste.Length; i++)
    {
        champs = liste[i].Split(séparateurs);
        dico[champs[0]] = int.Parse(champs[1]);
    }

// nbre d'éléments dans le dictionnaire
Console.WriteLine("Le dictionnaire a " + dico.Count + " éléments");

// liste des clés
Affiche("[Liste des clés]", dico.Keys);

// liste des valeurs
Affiche("[Liste des valeurs]", dico.Values);

// liste des clés & valeurs
Console.WriteLine("[Liste des clés & valeurs]");
foreach (string clé in dico.Keys)
  {
       Console.WriteLine("clé=" + clé + " valeur=" + dico[clé]);

  }


#region
void AfficherList()
{
    var listViewer = Enum.GetValues(typeof(ListViewer));

    foreach (var item in listViewer)
        {
            string itemTransforme = item.ToString().Replace("_", " ");
            Console.WriteLine("{0}: {1}", (int)item, itemTransforme);
        }

}
#endregion
