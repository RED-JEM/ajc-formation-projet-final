﻿using Newtonsoft.Json;


namespace Stream_infrastructure
{
    /// <summary>
    /// Classe de gestion de la sauvegarde en Json
    /// </summary>

    public class JsonViewerSaver : IViewerSaver
    {
        #region Fields
        private readonly string filePath;
        #endregion

        #region Constructors
        public JsonViewerSaver(string path)
        {
            this.filePath = path;
        }
        #endregion

        #region Public methods
        /// <summary>
        /// Methode qui permet la sauvegarde d'une partie au format Json
        /// </summary>
        /// <param name="obj"></param>
        public void SaveViewer(object ab)
        {
            string json = JsonConvert.SerializeObject(ab, Formatting.Indented);
            File.WriteAllText(FilePath, json);
        }
        #endregion

        #region Properties
        public string FilePath => filePath;

       
        #endregion
    }

}