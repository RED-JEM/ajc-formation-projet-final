﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Stream.api
{
    public class Connection
    {
        #region Fields
        public event Action<Connection, DateTime> ConnectionDate;
        private readonly IAfficheur afficheur;
        private readonly ILecture saisie;
        private readonly ISauvegardeTempsConnection saver;
        #endregion

        #region Properties
        public int Id { get; set; } = 0;
        public string Name { get; set; } = "";

        public List<Viewer> Viewer { get; set; } = new();

        public DateTime DateConnection { get; private set; } = DateTime.Now;

        public DateTime DateDebut { get; private set; } = DateTime.Now;

        #endregion

    }
}
