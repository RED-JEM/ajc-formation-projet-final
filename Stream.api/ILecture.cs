﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Stream.api
{
    /// <summary>
    /// Interface pour gerer la lecture des infos des viewers
    /// </summary>
    public interface ILecture
    {
        void lireViewers();
    }
}
