﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Stream.api
{
    /// <summary>
    /// Interface qui sert de patron à la sauvegarde d'un abonnee
    /// </summary>
    public interface IViewerSaver
    {
        public void SaveViewer(object ab);
    }
}
