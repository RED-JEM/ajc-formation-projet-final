﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.Json.Serialization;
using System.Threading.Tasks;

namespace Stream.api
{
    /// <summary>
    /// Liste des abonnees 
    /// </summary>
    public class ListViewer
    {
        /// <summary>
        /// Liste des abonnees order by temps de connections
        /// </summary>
        public int OrdreAffichage { get; set; }
    }
            
}

