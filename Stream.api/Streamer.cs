﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Giveaway.api;

namespace Stream.api
{
	public class Streamer : Profils
	{
        #region Attributs 
        private List<Viewer> viewer;
        private List<BaseCadeau> cadeau;
        private IAfficheur afficheur;
        #endregion


        #region Constructor
        public Streamer(int id, string username, string email, IAfficheur afficheur) : base(id, username, email)
            {
                this.Afficheur = afficheur;
            }
        #endregion


        #region  proporties
        public List<Viewer> Abonnee { get => viewer; set => viewer = value; }
        public List<BaseCadeau> CAdeau { get => cadeau; set => cadeau = value; }
        public IAfficheur Afficheur { get => afficheur; set => afficheur = value; }
        #endregion

    }
}
