﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.Json.Serialization;
using System.Threading.Tasks;
using System.Xml.Linq;

namespace Stream.api
{
	/// <summary>
    /// Classe représentant le Viewer
    /// </summary>
    public abstract class Viewer : Profils
    {
        #region Attribut
        private int id;
        private string username;
        private string email;
        #endregion


        #region constructor
        public Viewer(int id, string username, string email) : base(id, username, email)
        {
            this.Id = id;
            this.UserName = username;
            this.Email = email;
        }
        #endregion

        #region proporties
        public int Id { get => id; set => id = value; }
        public string UserName { get => username; set => username = value; }
        public string Email { get => email; set => email = value; }
        #endregion

    }


}











